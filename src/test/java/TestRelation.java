import com.shiro.components.Mesh;
import com.shiro.components.factories.MeshFactory;
import com.shiro.relations.SimpleRelation;
import com.shiro.nextGridSystem.Direction;
import org.junit.Test;

import static org.junit.Assert.*;
public class TestRelation {

    @Test
    public void equals() {

        MeshFactory cf = new MeshFactory();
        Mesh blueFlowers = cf.getBlueFlowers();
        Mesh bush = cf.getBush();
        Mesh est = cf.getR();
        Mesh grass = cf.getGrass();
        assertEquals(new SimpleRelation(blueFlowers, Direction.NORTH, bush), new SimpleRelation(bush, Direction.SOUTH, blueFlowers));
        assertEquals(new SimpleRelation(grass, Direction.WEST, est).hashCode(), new SimpleRelation(est, Direction.EAST, grass).hashCode());
    }
}
