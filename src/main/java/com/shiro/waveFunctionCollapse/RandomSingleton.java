package com.shiro.waveFunctionCollapse;

import lombok.Getter;

import java.util.Map;
import java.util.Random;

@Getter
public final class RandomSingleton {
    private static RandomSingleton instance =  null;
    private final Random random;

    private RandomSingleton() {
        random = new Random();
    }

    public static RandomSingleton getInstance() {
        if (instance == null) {
            instance = new RandomSingleton();
        }
        return instance;
    }

    public void setSeed(long seed) {
        random.setSeed(seed);
    }

    // from https://stackoverflow.com/questions/6737283/weighted-randomness-in-java
    public <E> E getWeightedRandom(Map<E, Double> weightedItems) {

        // Compute the total weight of all items together.
        // This can be skipped of course if sum is already 1.
        double totalWeight = weightedItems.values().stream().mapToDouble(v -> v).sum();

        // Now choose a random item.
        double r = random.nextDouble(1) * totalWeight;
        for (Map.Entry<E, Double> e : weightedItems.entrySet()) {
            r -= e.getValue();
            if (r <= 0.0)
                return e.getKey();
        }
        throw new RuntimeException();
    }

    public int nextInt(int size) {
        return random.nextInt(size);
    }
}
