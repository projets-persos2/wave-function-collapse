package com.shiro.waveFunctionCollapse;

import com.shiro.nextGridSystem.Cell;
import com.shiro.nextGridSystem.Grid;

public class WaveFunctionCollapser implements IWaveFunctionCollapser {
    private final Grid grid;
    private int chunkSize;

    public WaveFunctionCollapser(Grid grid, int chunkSize) {
        this.grid = grid;
        this.chunkSize = chunkSize;
    }

    @Override
    public void setup() {
        grid.updateEntropy();
    }

    @Override
    public void setup(long seed) {
        RandomSingleton r = RandomSingleton.getInstance();
        r.setSeed(seed);
        setup();
    }

    @Override
    public void complete() {
        Cell leastEntropyCell;
        while (grid.isUncompleted()) {
            leastEntropyCell = grid.pickOneOfLeastEntropyCells();
            if (leastEntropyCell.getEntropy() != 0) {
                leastEntropyCell.collapse();
                System.out.println("\n"+grid);
                System.out.println("Collapsing " + leastEntropyCell.getCoordinates());
                chunkSize = 1;
            }else {
                grid.resetChunk(chunkSize, leastEntropyCell.getCoordinates());
                System.out.println("\nReloading on " + leastEntropyCell.getCoordinates());
                System.out.println("\n"+grid);
                chunkSize++;
            }
            grid.propagateFrom(leastEntropyCell);
        }
    }


}
