package com.shiro.relations;

import com.shiro.components.Mesh;
import com.shiro.nextGridSystem.Direction;

import java.util.Optional;
import java.util.Set;

public interface IRelation {

    public Optional<Mesh> getPotentialInDirectionOf(Direction direction, Mesh mesh);

    Set<Mesh> getAll();
}
