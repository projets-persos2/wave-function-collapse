import com.shiro.waveFunctionCollapse.RandomSingleton;
import org.junit.Test;

import java.util.Map;

public class TestRandom {
    @Test
    public void halfTest() {
        RandomSingleton r = RandomSingleton.getInstance();

        r.getWeightedRandom(
                Map.of(
                        1, 0.5,
                        2, 0.5
                )
        );
    }
}
