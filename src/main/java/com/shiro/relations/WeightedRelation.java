package com.shiro.relations;

import com.shiro.components.Mesh;
import com.shiro.nextGridSystem.Direction;

import java.util.Optional;
import java.util.Set;

public class WeightedRelation implements IRelation {
    private final SimpleRelation simpleRelation;
    private final double weight;

    public WeightedRelation(Mesh _origin, Direction _direction, Mesh _head, double weight) {
        if (weight>1 || weight<0) throw new IllegalArgumentException("IAE : "+weight+" is not a valid value, weight mush be between 0 and 1 included");
        this.simpleRelation = new SimpleRelation(_origin, _direction, _head);
        this.weight = weight;
    }

    @Override
    public Optional<Mesh> getPotentialInDirectionOf(Direction direction, Mesh mesh) {
        return Optional.empty();
    }

    @Override
    public Set<Mesh> getAll() {
        return null;
    }

    @Override
    public String toString() {
        return simpleRelation +", w:" + weight;
    }
}
