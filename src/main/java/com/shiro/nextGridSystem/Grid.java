package com.shiro.nextGridSystem;

import com.shiro.components.Mesh;
import com.shiro.relations.RelationMap;
import com.shiro.waveFunctionCollapse.RandomSingleton;
import lombok.Data;

import java.util.*;
import java.util.stream.Collectors;

@Data
public class Grid {
    private final int height;
    private final int width;
    private final Mesh baseMesh;
    private final RelationMap relationMap;
    private HashMap<Coordinates, Cell> cells;

    public Grid(int width, int height, Mesh baseMesh, RelationMap relationMap) {
        this.height = height;
        this.width = width;
        this.baseMesh = baseMesh;
        this.relationMap = relationMap;
        this.cells = new HashMap<>();
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                cells.put(
                        new Coordinates(x, y),
                        new Cell(
                            baseMesh.clone(),
                            new Coordinates(x, y)
                        )
                );
            }
        }
    }

    public Boolean isInGrid(Coordinates _toCompare) {
        return _toCompare.getX() >= 0 && _toCompare.getY() >= 0 && _toCompare.getX() < width && _toCompare.getY() < height;
    }

    public Set<Coordinates> getSurroundingCoordinates(int radius, Coordinates coordinates) {
        return coordinates.getSurroundingCoordinates(radius).stream().filter(this::isInGrid).collect(Collectors.toSet());
    }

    public List<Cell> getSortedCellsByLineIndex(int index) {
        if (index < 0 || index >= height) throw new IndexOutOfBoundsException();
        return cells.values().stream().filter(cell -> cell.inOnLineIndex(index)).sorted().toList();
    }

    public Cell getCellAt(Coordinates coordinates) {
        if (!isInGrid(coordinates)) throw new IndexOutOfBoundsException();
        return cells.get(coordinates);
    }

    public void setMeshAt(Mesh _toSet, Coordinates coordinates) {
        getCellAt(coordinates).setMesh(_toSet);
    }

    public Collection<Cell> getAllCells() {
        return cells.values();
    }

    public Collection<Cell> getAllMeshedCells() {
        return cells.values().stream().filter(cell -> !cell.getMesh().equals(baseMesh)).collect(Collectors.toSet());
    }

    public void updateEntropy() {
        for (Cell c : getAllCells()) {
            c.setPossibilities(Set.copyOf(getPossibleMeshesFor(c)));
        }
    }

    public Set<Mesh> getPossibleMeshesFor(Cell c) {
        Set<Mesh> possibleList = new HashSet<>(Set.copyOf(relationMap.getAll()));
        Set<Neighbour> neighbours = getDirectMeshedNeighbours(c);

        for (Neighbour neighbour : neighbours) {
            possibleList.retainAll(relationMap.getRelationsDirectionOf(
                    neighbour.getNeighboursDirection().getOpposite(),
                    neighbour.getCell().getMesh()
            ));
        }
        return possibleList;
    }


    public Set<Neighbour> getDirectMeshedNeighbours(Cell c) {
        Set<Neighbour> neighbours = new HashSet<>();
        for (Direction d : Direction.values()) {
            Coordinates tempCo = c.getCoordinates().clone(d);
            if (isInGrid(tempCo)) {
                Cell cell = getCellAt(tempCo);
                if (!cell.getMesh().equals(baseMesh))
                    neighbours.add(new Neighbour(
                            getCellAt(tempCo),
                            d
                    ));
            }
        }
        return neighbours;
    }

    public Cell pickOneOfLeastEntropyCells() {
        int minEntropy = -1;
        List<Cell> lec = new ArrayList<>();
        for (Cell c : getAllCells().stream().filter(cell -> cell.getMesh().equals(baseMesh)).toList()) {
            if ((minEntropy == -1 || c.getEntropy() < minEntropy) && c.getMesh().equals(baseMesh)) {
                lec.clear();
                minEntropy = c.getEntropy();
                lec.add(c);
            }
            if (minEntropy == c.getEntropy()) {
                lec.add(c);
            }
        }
        RandomSingleton r = RandomSingleton.getInstance();
        return lec.get(r.nextInt(lec.size()));
    }

    public void propagateFrom(Cell c) {
        Set<Cell> cells4nextTurn =
            c.getCoordinates().getDirectNeighboursCoordinates().stream()
                .filter(this::isInGrid)
                .map(this::getCellAt)
                .filter(cell -> cell.getMesh().equals(baseMesh))
                .collect(Collectors.toSet()
        );
        System.out.println(c.getCoordinates().toString() + " -Propagate-> " + cells4nextTurn.stream().map(cell -> cell.getCoordinates().toString() + " ").collect(Collectors.joining()));
        propagateFromCells(cells4nextTurn);
    }

    public void propagateFromCells(Set<Cell> cells) {
        if (cells.isEmpty()) return;
        Set<Cell> cells4nextTurn = new HashSet<>();
        int tempEntropy;
        for (Cell c : cells) {
            tempEntropy = c.getEntropy();
            c.setPossibilities(getPossibleMeshesFor(c));
            System.out.println(c.getCoordinates().toString() + " Entropy: " +tempEntropy+ "-> " + c.getEntropy() + c.getPossibilities().stream().map(Mesh::getLabel).collect(Collectors.joining()));
            if (tempEntropy != c.getEntropy()) {
                cells4nextTurn.addAll(
                        c.getCoordinates().getDirectNeighboursCoordinates().stream()
                                .filter(this::isInGrid)
                                .map(this::getCellAt)
                                .filter(cell -> cell.getMesh().equals(baseMesh))
                                .collect(Collectors.toSet()
                                )
                );
            }
            if(!cells4nextTurn.isEmpty()) System.out.println(c.getCoordinates().toString() + " -Propagate-> " + cells4nextTurn.stream().map(cell -> cell.getCoordinates().toString() + " - ").collect(Collectors.joining()));
        }
        propagateFromCells(cells4nextTurn);
    }

    public void resetChunk(int chunkRadius, Coordinates coordinates) {
        for (Coordinates _coToReset : getSurroundingCoordinates(chunkRadius, coordinates)) {
            resetAt(_coToReset);
        }
    }

    public void resetAt(Coordinates _co) {
        this.setMeshAt(baseMesh, _co);
    }

    public boolean isUncompleted() {
        return cells.values().stream().anyMatch(cell -> cell.getMesh().equals(baseMesh));
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Grid : height=").append(height).append(", width=").append(width);
        for (int i = 0; i < height; i++) {
            sb.append('\n');
            for (Cell cell : getSortedCellsByLineIndex(i)) {
                sb.append(cell.toString());
            }
        }
        return sb.toString();
    }
}
