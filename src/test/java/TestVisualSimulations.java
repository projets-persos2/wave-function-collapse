import com.shiro.components.EmptyMesh;
import com.shiro.components.Mesh;
import com.shiro.components.factories.MeshFactory;
import com.shiro.relations.RelationMap;
import com.shiro.relations.SimpleRelation;
import com.shiro.relations.factories.RelationFactory;
import com.shiro.nextGridSystem.Coordinates;
import com.shiro.nextGridSystem.Direction;
import com.shiro.nextGridSystem.Grid;
import com.shiro.waveFunctionCollapse.WaveFunctionCollapser;
import com.shiro.waveFunctionCollapse.IWaveFunctionCollapser;
import org.junit.Test;

public class TestVisualSimulations {
    @Test
    public void ArrowsSimulation() {
        MeshFactory mf = new MeshFactory();
        RelationFactory rf = new RelationFactory();

        Mesh grass = mf.getGrass();
        Mesh dot = mf.getDot();
        Mesh o = mf.getL();
        Mesh e = mf.getR();
        Mesh n = mf.getU();
        Mesh s = mf.getD();
        Grid g = new Grid(5, 3, new EmptyMesh(), new RelationMap());
        g.setMeshAt(grass, new Coordinates(0, 0));
        g.setMeshAt(grass, new Coordinates(2, 0));
        g.setMeshAt(grass, new Coordinates(0, 2));
        g.setMeshAt(grass, new Coordinates(2, 2));
        g.setMeshAt(dot, new Coordinates(1, 1));
        g.setMeshAt(n, new Coordinates(1, 0));
        g.setMeshAt(s, new Coordinates(1, 2));
        g.setMeshAt(o, new Coordinates(0, 1));
        g.setMeshAt(e, new Coordinates(2, 1));
        g.setMeshAt(s, new Coordinates(3, 0));
        g.setMeshAt(grass, new Coordinates(3, 1));
        g.setMeshAt(n, new Coordinates(3, 2));
        g.setMeshAt(o, new Coordinates(4, 1));


        RelationMap r = new RelationMap(rf.inferFromGrid(g));
        r.addRelation(new SimpleRelation(grass, Direction.NORTH, grass));
        r.addRelation(new SimpleRelation(grass, Direction.SOUTH, grass));
        r.addRelation(new SimpleRelation(grass, Direction.EAST, grass));
        r.addRelation(new SimpleRelation(grass, Direction.WEST, grass));
        Grid grid = new Grid(15, 15, new EmptyMesh(), r);

        IWaveFunctionCollapser wfc = new WaveFunctionCollapser(grid, 2);
        wfc.setup();
        wfc.complete();
        System.out.println(grid);

    }

    @Test
    public void WeightedBeach() {}
    @Test
    public void FlowerRockAndBush() {
        MeshFactory cf = new MeshFactory();
        RelationFactory rf = new RelationFactory();
        RelationMap rm = new RelationMap();

        Mesh grass = cf.getGrass();
        Mesh smallRock = cf.getSmallRock();
        Mesh rock = cf.getRockComponent();
        Mesh blueFlowers = cf.getBlueFlowers();
        Mesh yellowFlowers = cf.getYellowFlowers();
        Mesh bush = cf.getBush();

        rm.addAllRelations(rf.relate(grass).with(grass).anyDirections().build());
        rm.addAllRelations(rf.relate(grass).with(smallRock).anyDirections().build());
        rm.addAllRelations(rf.relate(grass).with(rock).anyDirections().build());
        rm.addAllRelations(rf.relate(grass).with(blueFlowers).anyDirections().build());
        rm.addAllRelations(rf.relate(grass).with(yellowFlowers).anyDirections().build());
        rm.addAllRelations(rf.relate(grass).with(bush).anyDirections().build());
        rm.addAllRelations(rf.relate(yellowFlowers).with(yellowFlowers).anyDirections().build());
        rm.addAllRelations(rf.relate(yellowFlowers).with(blueFlowers).anyDirections().build());
        rm.addAllRelations(rf.relate(smallRock).with(rock).anyDirections().build());

        rm.addRelation(rf.relate(yellowFlowers).with(bush).onHis(Direction.NORTH).build());
        rm.addRelation(rf.relate(yellowFlowers).with(bush).onHis(Direction.SOUTH).build());
        rm.addRelation(rf.relate(blueFlowers).with(bush).onHis(Direction.EAST).build());
        rm.addRelation(rf.relate(blueFlowers).with(bush).onHis(Direction.WEST).build());

        Grid grid = new Grid(25, 15, new EmptyMesh(), rm);

        IWaveFunctionCollapser wfc = new WaveFunctionCollapser(grid, 1);
        wfc.setup(2);
        wfc.complete();
        System.out.println(grid);

    }
    @Test
    public void Beach() {
        MeshFactory mf = new MeshFactory();
        RelationFactory rf = new RelationFactory();
        RelationMap rm = new RelationMap();
        Mesh grass = mf.getGrass();
        Mesh water = mf.getWater();
        Mesh beach = mf.getBeach();
        Mesh bush = mf.getBush();

        rm.addAllRelations(rf.relate(grass).with(grass).anyDirections().build());
        rm.addAllRelations(rf.relate(water).with(water).anyDirections().build());
        rm.addAllRelations(rf.relate(beach).with(beach).anyDirections().build());
        rm.addAllRelations(rf.relate(bush).with(bush).anyDirections().build());
        rm.addAllRelations(rf.relate(beach).with(water).anyDirections().build());
        rm.addAllRelations(rf.relate(beach).with(grass).anyDirections().build());
        rm.addAllRelations(rf.relate(beach).with(water).anyDirections().build());
        rm.addAllRelations(rf.relate(bush).with(grass).anyDirections().build());

        Grid grid = new Grid(25, 10, new EmptyMesh(), rm);
        grid.setMeshAt(water, new Coordinates(3, 5));
        IWaveFunctionCollapser wfc = new WaveFunctionCollapser(grid, 1);
        wfc.setup();
        wfc.complete();
        System.out.println(grid);

    }
}