package com.shiro.nextGridSystem;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Neighbour {
    private Cell cell;
    private Direction neighboursDirection;

}
