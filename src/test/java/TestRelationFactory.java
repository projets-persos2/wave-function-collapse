import com.shiro.components.EmptyMesh;
import com.shiro.components.Mesh;
import com.shiro.nextGridSystem.Direction;
import com.shiro.relations.IRelation;
import com.shiro.relations.RelationMap;
import com.shiro.relations.SimpleRelation;
import com.shiro.relations.WeightedRelation;
import com.shiro.relations.factories.RelationFactory;
import com.shiro.components.factories.MeshFactory;
import com.shiro.nextGridSystem.Coordinates;
import com.shiro.nextGridSystem.Grid;
import org.junit.Test;

import java.util.Set;

public class TestRelationFactory {

    @Test
    public void lesFleches() {
        MeshFactory mf = new MeshFactory();
        RelationFactory rf = new RelationFactory();

        Mesh grass = mf.getGrass();
        Mesh dot = mf.getDot();
        Mesh o = mf.getL();
        Mesh e = mf.getR();
        Mesh n = mf.getU();
        Mesh s = mf.getD();

        Grid g = new Grid(5, 3, new EmptyMesh(), new RelationMap());
        g.setMeshAt(grass, new Coordinates(0, 0));
        g.setMeshAt(grass, new Coordinates(2, 0));
        g.setMeshAt(grass, new Coordinates(0, 2));
        g.setMeshAt(grass, new Coordinates(2, 2));
        g.setMeshAt(dot, new Coordinates(1, 1));
        g.setMeshAt(n, new Coordinates(1, 0));
        g.setMeshAt(s, new Coordinates(1, 2));
        g.setMeshAt(o, new Coordinates(0, 1));
        g.setMeshAt(e, new Coordinates(2, 1));
        g.setMeshAt(s, new Coordinates(3, 0));
        g.setMeshAt(grass, new Coordinates(3, 1));
        g.setMeshAt(n, new Coordinates(3, 2));
        g.setMeshAt(o, new Coordinates(4, 1));

        System.out.println(g);
        System.out.println(rf.inferFromGrid(g).size());
        for (IRelation iRelation : rf.inferFromGrid(g)) {
            System.out.println(iRelation);
        }
    }

    @Test
    public void testCreationRelation() {
        MeshFactory mf = new MeshFactory();
        RelationFactory rf = new RelationFactory();

        Mesh grass = mf.getGrass();
        Mesh dot = mf.getDot();

        RelationMap rm = new RelationMap();
        rm.addRelation(rf.relate(grass).with(dot).onHis(Direction.NORTH).build());
        rm.addAllRelations(rf.relate(grass).with(dot).onHis(Direction.NORTH).and(Direction.SOUTH).build());
        rm.addRelation(rf.relate(grass).with(dot).onHis(Direction.NORTH).withChance(.5).build());
        System.out.println(rm);
    }
}
