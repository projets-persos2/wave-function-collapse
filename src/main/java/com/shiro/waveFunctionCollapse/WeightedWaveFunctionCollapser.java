package com.shiro.waveFunctionCollapse;

import com.shiro.nextGridSystem.Cell;
import com.shiro.nextGridSystem.Coordinates;
import com.shiro.nextGridSystem.Grid;

public class WeightedWaveFunctionCollapser {
    private final Grid grid;
    private final int chunkSize;

    public WeightedWaveFunctionCollapser(Grid grid, int chunkSize) {
        this.grid = grid;
        this.chunkSize = chunkSize;
    }

    public void setup() {
        grid.updateEntropy();
    }

    public void complete() {
        Cell leastEntropyCell;
        while (grid.isUncompleted()) {
            leastEntropyCell = grid.pickOneOfLeastEntropyCells();
            if (leastEntropyCell.getEntropy() != 0) {
                leastEntropyCell.collapse();
                System.out.println("\n"+grid);
                System.out.println("Collapsing " + leastEntropyCell.getCoordinates());
            }else {
                System.out.println("\nReloading on " + leastEntropyCell.getCoordinates());
                resetChunk(leastEntropyCell.getCoordinates());
            }
            grid.propagateFrom(leastEntropyCell);
        }
    }

    private void resetChunk(Coordinates coordinates) {
        for (Coordinates surroundingCoordinate : grid.getSurroundingCoordinates(chunkSize, coordinates)) {
            grid.resetAt(surroundingCoordinate);
        }
    }


}
