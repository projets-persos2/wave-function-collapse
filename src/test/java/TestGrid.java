import com.shiro.components.factories.MeshFactory;
import com.shiro.relations.RelationMap;
import com.shiro.nextGridSystem.Coordinates;
import com.shiro.nextGridSystem.Grid;

public class TestGrid {
    public static void main(String[] args) {
        MeshFactory cf = new MeshFactory();

        Grid grid = new Grid(20, 10, cf.getGrass(), new RelationMap());
        System.out.println(grid);
        grid.setMeshAt(cf.getRockComponent(), new Coordinates(1, 1));
        System.out.println(grid);
        grid.setMeshAt(cf.getRockComponent(), new Coordinates(4, 1));
        System.out.println(grid);

        for (Coordinates surrondingCoordinate : grid.getSurroundingCoordinates(3, new Coordinates(15, 9))) {
            grid.setMeshAt(cf.getBlueFlowers(), surrondingCoordinate);
        }
        System.out.println(grid);

    }
}
