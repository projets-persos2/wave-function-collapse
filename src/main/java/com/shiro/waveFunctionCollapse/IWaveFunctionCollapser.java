package com.shiro.waveFunctionCollapse;

public interface IWaveFunctionCollapser {
    void setup();
    void setup(long seed);

    void complete();

}
