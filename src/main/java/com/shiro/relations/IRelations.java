package com.shiro.relations;

import com.shiro.components.Mesh;
import com.shiro.nextGridSystem.Direction;

import java.util.Set;

public interface IRelations {
    public void addRelation(IRelation potential);
    public Set<Mesh> getRelationsDirectionOf(Direction direction, Mesh mesh);

    Set<Mesh> getAll();

    void addAllRelations(Set<SimpleRelation> build);
}
