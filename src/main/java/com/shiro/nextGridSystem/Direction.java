package com.shiro.nextGridSystem;

import lombok.Getter;

public enum Direction {
    NORTH(0, -1, "verticale"),
    SOUTH(0, 1, "verticale"),
    EAST(1, 0, "horizontal"),
    WEST(-1, 0, "horizontal");

    private final int xOffset;
    private final int yOffset;
    @Getter
    private final String axis;

    Direction(int xOffset, int yOffset, String axis) {
        this.xOffset = xOffset;
        this.yOffset = yOffset;
        this.axis = axis;
    }

    public Direction getOpposite() {
        switch (this) {
            case NORTH:
                return SOUTH;
            case SOUTH:
                return NORTH;
            case EAST:
                return WEST;
            case WEST:
                return EAST;
            default:
                throw new RuntimeException("Unexpected direction");
        }
    }

    public int getXOffset() {
        return xOffset;
    }

    public int getYOffset() {
        return yOffset;
    }

}