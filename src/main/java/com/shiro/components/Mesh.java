package com.shiro.components;

import com.shiro.relations.IRelations;
import com.shiro.relations.RelationMap;
import lombok.Getter;
import lombok.Setter;

import java.util.*;

@Getter
@Setter
public class Mesh implements Cloneable{
    private int id;
    private IRelations potentials;
    private String label;
    private String name;

    public Mesh(int id, String label, String name) {
        this.id = id;
        this.label = label;
        this.name = name;
        this.potentials = new RelationMap();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Mesh that = (Mesh) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }


    @Override
    public Mesh clone() {
        try {
            Mesh clone = (Mesh) super.clone();
            clone.setId(this.id);
            clone.setLabel(this.label);
            clone.setName(this.name);
            return clone;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();
        }
    }

    @Override
    public String toString() {
        return "mesh" +" : " + id + ", " + label + ", " + name;
    }
}
