package com.shiro.components.factories;

import com.shiro.components.Mesh;

public class MeshFactory {
    private final Mesh grassComponent;
    private final Mesh smallRockComponent;
    private final Mesh rockComponent;
    private final Mesh yellowFlower;
    private final Mesh blueFlower;
    private final Mesh bush;
    private final Mesh dot;
    private final Mesh l;
    private final Mesh r;
    private final Mesh u;
    private final Mesh d;
    private final Mesh beach;
    private final Mesh water;

    public MeshFactory() {
        grassComponent = new Mesh(1, "\uD83D\uDFE9", "Grass");
        smallRockComponent = new Mesh(2, "\uD83E\uDEA8", "Small rock");
        rockComponent = new Mesh(3, "⛰️", "Rock");
        yellowFlower = new Mesh(4, "\uD83C\uDF3B", "Yellow flower");
        blueFlower = new Mesh(5, "\uD83E\uDEBB", "Blue flower");
        bush = new Mesh(6, "\uD83C\uDF3F", "bush");
        dot = new Mesh(7, "\uD83D\uDD35", "dot");
        l = new Mesh(8, "⬅️", "l");
        r = new Mesh(9, "➡️", "r");
        u = new Mesh(10, "⬆️", "u");
        d = new Mesh(11, "⬇️", "d");
        beach = new Mesh(12, "\uD83D\uDFE8", "beach");
        water = new Mesh(13, "\uD83D\uDFE6", "water");

    }

    public Mesh getGrass() {
        return grassComponent.clone();
    }

    public Mesh getSmallRock() {
        return smallRockComponent.clone();
    }

    public Mesh getRockComponent() {
        return rockComponent.clone();
    }

    public Mesh getBlueFlowers() {
        return blueFlower.clone();
    }

    public Mesh getYellowFlowers() {
        return yellowFlower.clone();
    }

    public Mesh getDot() {
        return dot.clone();
    }

    public Mesh getL() {
        return l.clone();
    }

    public Mesh getR() {
        return r.clone();
    }

    public Mesh getU() {
        return u.clone();
    }

    public Mesh getD() {
        return d.clone();
    }

    public Mesh getBush() {
        return bush.clone();
    }

    public Mesh getWater() {
        return water.clone();
    }

    public Mesh getBeach() {
        return beach.clone();
    }
}
