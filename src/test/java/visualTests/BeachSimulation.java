package visualTests;

import com.shiro.components.EmptyMesh;
import com.shiro.components.Mesh;
import com.shiro.components.factories.MeshFactory;
import com.shiro.nextGridSystem.Coordinates;
import com.shiro.nextGridSystem.Grid;
import com.shiro.relations.RelationMap;
import com.shiro.relations.factories.RelationFactory;
import com.shiro.waveFunctionCollapse.WaveFunctionCollapser;
import com.shiro.waveFunctionCollapse.IWaveFunctionCollapser;

import java.util.Set;

public class BeachSimulation {
    public static void main(String[] args) {
        MeshFactory mf = new MeshFactory();
        RelationFactory rf = new RelationFactory();
        RelationMap rm = new RelationMap();
        Mesh grass = mf.getGrass();
        Mesh water = mf.getWater();
        Mesh beach = mf.getBeach();
        Mesh bush = mf.getBush();

        rm.addAllRelations(rf.relate(grass).with(grass).anyDirections().build());
        rm.addAllRelations(rf.relate(water).with(water).anyDirections().build());
        rm.addAllRelations(rf.relate(beach).with(beach).anyDirections().build());
        rm.addAllRelations(rf.relate(bush).with(bush).anyDirections().build());
        rm.addAllRelations(rf.relate(beach).with(water).anyDirections().build());
        rm.addAllRelations(rf.relate(beach).with(grass).anyDirections().build());
        rm.addAllRelations(rf.relate(beach).with(water).anyDirections().build());
        rm.addAllRelations(rf.relate(bush).with(grass).anyDirections().build());

        Grid grid = new Grid(25, 10, new EmptyMesh(), rm);
        grid.setMeshAt(water, new Coordinates(3, 5));
        IWaveFunctionCollapser wfc = new WaveFunctionCollapser(grid, 1);
        wfc.setup();
        wfc.complete();
        System.out.println(grid);

    }
}