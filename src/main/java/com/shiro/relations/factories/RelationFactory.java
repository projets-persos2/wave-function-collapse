package com.shiro.relations.factories;

import com.shiro.components.Mesh;
import com.shiro.nextGridSystem.Direction;
import com.shiro.relations.IRelation;
import com.shiro.relations.SimpleRelation;
import com.shiro.nextGridSystem.Cell;
import com.shiro.nextGridSystem.Grid;
import com.shiro.nextGridSystem.Neighbour;
import com.shiro.relations.WeightedRelation;

import java.security.cert.CertPathBuilder;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class RelationFactory {
    public Set<IRelation> inferFromGrid(Grid grid) {
        Set<IRelation> relations = new HashSet<>();
        for (Cell cell : grid.getAllMeshedCells()) {
            for (Neighbour directMeshedNeighbour : grid.getDirectMeshedNeighbours(cell)) {
                relations.add(new SimpleRelation(
                        cell.getMesh(),
                        directMeshedNeighbour.getNeighboursDirection(),
                        directMeshedNeighbour.getCell().getMesh()
                ));
            }
        }
        return relations;
    }

    public MeshToRelate relate(Mesh grass) {
        return new MeshToRelate(grass);
    }

    public class MeshToRelate {

        private final Mesh originMesh;

        public MeshToRelate(Mesh originMesh) {
            this.originMesh = originMesh;
        }

        public MeshCouple with(Mesh _mesh) {
            return new MeshCouple(this.originMesh, _mesh);
        }
    }

    public class MeshCouple {
        private final Mesh originMesh;
        private final Mesh headMesh;
        public MeshCouple(Mesh originMesh, Mesh headMesh) {
            this.originMesh = originMesh;
            this.headMesh = headMesh;
        }

        public DirectedRelation onHis(Direction direction) {
            return new DirectedRelation(originMesh, direction, headMesh);
        }

        public MultipleDirectedRelation anyDirections() {
            return new MultipleDirectedRelation(originMesh, Arrays.stream(Direction.values()).collect(Collectors.toSet()), headMesh);
        }
    }

    public class DirectedRelation {
        private final Mesh originMesh;
        private final Direction direction;
        private final Mesh headMesh;

        public DirectedRelation(Mesh originMesh, Direction direction, Mesh headMesh) {
            this.originMesh = originMesh;
            this.direction = direction;
            this.headMesh = headMesh;
        }

        public MultipleDirectedRelation and(Direction _direction) {
            return new MultipleDirectedRelation(
                    originMesh,
                    Set.of(this.direction, _direction),
                    headMesh
            );
        }

        public SimpleRelation build() {
            return new SimpleRelation(
                    originMesh,
                    direction,
                    headMesh
            );
        }

        public WeightedDirectedRelation withChance(double weight) {
            return new WeightedDirectedRelation(
                    originMesh,
                    direction,
                    headMesh,
                    weight
            );
        }
    }

    public class WeightedDirectedRelation {
        private final Mesh originMesh;
        private final Direction direction;
        private final Mesh headMesh;
        private final double weight;

        public WeightedDirectedRelation(Mesh originMesh, Direction direction, Mesh headMesh, double weight) {
            this.originMesh = originMesh;
            this.direction = direction;
            this.headMesh = headMesh;
            this.weight = weight;
        }

        public WeightedRelation build() {
            return new WeightedRelation(
                    originMesh,
                    direction,
                    headMesh,
                    weight
            );
        }
    }

    public class MultipleDirectedRelation {

        private final Mesh originMesh;
        private final Set<Direction> directions;
        private final Mesh headMesh;

        public MultipleDirectedRelation(Mesh originMesh, Set<Direction> directions, Mesh headMesh) {
            this.originMesh = originMesh;
            this.directions = directions;
            this.headMesh = headMesh;
        }

        public MultipleDirectedRelation and(Direction _direction) {
            directions.add(_direction);
            return this;
        }

        public Set<SimpleRelation> build() {
            return directions.stream()
                .map(direction -> new SimpleRelation(originMesh, direction, headMesh))
                .collect(Collectors.toSet());
        }
    }


}
