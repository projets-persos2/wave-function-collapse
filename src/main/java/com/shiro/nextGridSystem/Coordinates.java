package com.shiro.nextGridSystem;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.*;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
public class Coordinates implements Cloneable, Comparable<Coordinates> {
    private int x;
    private int y;

    public Set<Coordinates> getSurroundingCoordinates(int radius) {
        Set<Coordinates> co = new HashSet<>();
        for (int xOffset = -radius; xOffset <= radius; xOffset++) {
            for (int yOffset = -radius; yOffset <= radius; yOffset++) {
                co.add(new Coordinates(x+xOffset, y+yOffset));
            }
        }
        return co;
    }
    @Override
    public int compareTo(Coordinates c2) {
        if (this.getY() == c2.getY()) {
            return this.getX()-c2.getX();
        }
        return this.getY()-c2.getY();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coordinates that = (Coordinates) o;
        return x == that.x && y == that.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    public Set<Coordinates> getDirectNeighboursCoordinates() {
        return Arrays.stream(Direction.values())
                .map(this::clone)
                .collect(Collectors.toSet());
    }

    @Override
    public Coordinates clone() {
        try {
            Coordinates clone = (Coordinates) super.clone();
            clone.setX(this.x);
            clone.setY(this.y);
            return clone;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();
        }
    }



    public Coordinates clone(Direction d) {
        Coordinates coordinates = this.clone();
        coordinates.applyOffset(d);
        return coordinates;
    }

    private void applyOffset(Direction d) {
        this.setX(x+d.getXOffset());
        this.setY(y+d.getYOffset());
    }

    @Override
    public String toString() {
        return x +", " + y;
    }
}
