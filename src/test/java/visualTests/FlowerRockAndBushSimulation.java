package visualTests;

import com.shiro.components.EmptyMesh;
import com.shiro.components.Mesh;
import com.shiro.components.factories.MeshFactory;
import com.shiro.nextGridSystem.Direction;
import com.shiro.nextGridSystem.Grid;
import com.shiro.relations.RelationMap;
import com.shiro.relations.factories.RelationFactory;
import com.shiro.waveFunctionCollapse.WaveFunctionCollapser;
import com.shiro.waveFunctionCollapse.RandomSingleton;
import com.shiro.waveFunctionCollapse.IWaveFunctionCollapser;

import java.util.Set;

public class FlowerRockAndBushSimulation {
    public static void main(String[] args) {
        MeshFactory cf = new MeshFactory();
        RelationFactory rf = new RelationFactory();
        RelationMap rm = new RelationMap();

        Mesh grass = cf.getGrass();
        Mesh smallRock = cf.getSmallRock();
        Mesh rock = cf.getRockComponent();
        Mesh blueFlowers = cf.getBlueFlowers();
        Mesh yellowFlowers = cf.getYellowFlowers();
        Mesh bush = cf.getBush();

        rm.addAllRelations(rf.relate(grass).with(grass).anyDirections().build());
        rm.addAllRelations(rf.relate(grass).with(smallRock).anyDirections().build());
        rm.addAllRelations(rf.relate(grass).with(rock).anyDirections().build());
        rm.addAllRelations(rf.relate(grass).with(blueFlowers).anyDirections().build());
        rm.addAllRelations(rf.relate(grass).with(yellowFlowers).anyDirections().build());
        rm.addAllRelations(rf.relate(grass).with(bush).anyDirections().build());
        rm.addAllRelations(rf.relate(yellowFlowers).with(yellowFlowers).anyDirections().build());
        rm.addAllRelations(rf.relate(yellowFlowers).with(blueFlowers).anyDirections().build());
        rm.addAllRelations(rf.relate(smallRock).with(rock).anyDirections().build());

        rm.addRelation(rf.relate(yellowFlowers).with(bush).onHis(Direction.NORTH).build());
        rm.addRelation(rf.relate(yellowFlowers).with(bush).onHis(Direction.SOUTH).build());
        rm.addRelation(rf.relate(blueFlowers).with(bush).onHis(Direction.EAST).build());
        rm.addRelation(rf.relate(blueFlowers).with(bush).onHis(Direction.WEST).build());

        Grid grid = new Grid(25, 15, new EmptyMesh(), rm);

        IWaveFunctionCollapser wfc = new WaveFunctionCollapser(grid, 1);
        wfc.setup(2);
        wfc.complete();
        System.out.println(grid);

    }
}