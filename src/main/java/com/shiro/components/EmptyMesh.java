package com.shiro.components;


public class EmptyMesh extends Mesh {

    public static final int EMPTY_COMPONENT_ID = 0;
    public EmptyMesh() {
        super(EMPTY_COMPONENT_ID, "  ", "Empty space");
    }
}
