package com.shiro.relations;

import com.shiro.components.Mesh;
import com.shiro.nextGridSystem.Direction;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Objects;
import java.util.Optional;
import java.util.Set;

@Data
@AllArgsConstructor
public class SimpleRelation implements IRelation {
    private final Mesh origin;
    private final Direction direction;
    private final Mesh to;


    public Optional<Mesh> getPotentialInDirectionOf(Direction direction, Mesh mesh) {
        if (direction.equals(this.direction)) {
            return mesh.equals(origin) ? Optional.of(to) : Optional.empty();
        }
        if (direction.getOpposite().equals(this.direction)) {
            return mesh.equals(to) ? Optional.of(origin) : Optional.empty();
        }
        return Optional.empty();
    }

    @Override
    public Set<Mesh> getAll() {
        Set<Mesh> s = new java.util.HashSet<>(Set.of(origin));
        s.add(to);
        return s;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SimpleRelation that = (SimpleRelation) o;
        if (direction.equals(that.direction))
            return Objects.equals(origin, that.origin) && Objects.equals(to, that.to);

        if (direction.getOpposite().equals(that.direction))
            return Objects.equals(origin, that.to) && Objects.equals(to, that.origin);

        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(direction.getAxis(), origin.getId()>=to.getId() ? origin: to, origin.getId()<to.getId() ? to : origin  );
    }

    @Override
    public String toString() {
        return origin.getLabel() + "-" + direction + "-" + to.getLabel();
    }
}
