package com.shiro.nextGridSystem;

import com.shiro.components.Mesh;
import com.shiro.waveFunctionCollapse.RandomSingleton;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Objects;
import java.util.Set;

@Getter
public class Cell implements Comparable<Cell> {
    @Setter
    private Mesh mesh;
    private final Coordinates coordinates;
    @Setter
    private Set<Mesh> possibilities;

    public Cell(Mesh mesh, Coordinates coordinates) {
        this.mesh = mesh;
        this.coordinates = coordinates;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cell cell = (Cell) o;
        return Objects.equals(mesh, cell.mesh) && Objects.equals(coordinates, cell.coordinates);
    }

    @Override
    public int hashCode() {
        return Objects.hash(coordinates);
    }

    public boolean inOnLineIndex(int index) {
        return this.coordinates.getY() == index;
    }


    @Override
    public int compareTo(Cell cell) {
        return this.coordinates.compareTo(cell.getCoordinates());
    }

    public int getEntropy() {
        return possibilities.size();
    }

    public void collapse() {
        this.mesh =  possibilities.stream().toList().get(RandomSingleton.getInstance().nextInt(possibilities.size()));
    }

    @Override
    public String toString() {
        return mesh.getLabel();
    }
}
