package com.shiro.relations;

import com.shiro.components.Mesh;
import com.shiro.nextGridSystem.Direction;

import java.util.*;
import java.util.stream.Collectors;

public class RelationMap implements IRelations {
    private final Set<IRelation> relations;

    public RelationMap() {
        this.relations = new HashSet<>();
    }

    public RelationMap(Set<IRelation> simpleRelations) {
        this.relations = simpleRelations;
    }

    @Override
    public void addRelation(IRelation relation) {
        this.relations.add(relation);
    }

    @Override
    public void addAllRelations(Set<SimpleRelation> _relations) {
        relations.addAll(_relations);
    }

    @Override
    public Set<Mesh> getRelationsDirectionOf(Direction direction, Mesh mesh) {
        return relations.stream()
                .map(iRelation -> iRelation.getPotentialInDirectionOf(direction, mesh))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<Mesh> getAll() {
        return relations.stream().map(IRelation::getAll).flatMap(Collection::stream).collect(Collectors.toSet());
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("RelationMap: \n");
        relations.forEach(relation -> sb.append(relation).append('\n'));
        return sb.toString();
    }
}
